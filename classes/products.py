import os
import sys
sys.path.append(os.getcwd())

from database.db import cursor, db
from flask import jsonify
from flask_restful import Resource, request, abort

class Products(Resource):
    
    def get(cls):
        cursor.execute('SELECT * FROM products;')
        sql_result = cursor.fetchall()
        result = []
        for product in sql_result:
            product_obj = {
                "productCode": product[0],
                "productName": product[1],
                "productLine": product[2],
                "productScale": product[3],
                "productVendor": product[4],
                "productDescription": product[5],
                "quantityInStock": product[6],
                "buyPrice": float(product[7]),
                "MSRP": float(product[8])
            }
            result.append(product_obj)
        return result, 200
    
    @staticmethod
    def abort_if_product_doesnt_exist(product_id):
        cursor.execute('SELECT * FROM products WHERE productCode=%s', (product_id, ))
        exists = cursor.fetchall()
        if not exists:
            abort(404, message = 'Product does not exist...')

    @staticmethod
    def abort_if_product_exists(product_id):
        cursor.execute('SELECT * FROM products WHERE productCode=%s', (product_id, ))
        exists = cursor.fetchall()
        if exists:
            abort(400, message = 'This productCode already exists...')

    @staticmethod
    def abort_if_product_line_doesnt_exist(product_line_id):
        cursor.execute('SELECT * FROM productlines WHERE productLine=%s', (product_line_id, ))
        exists = cursor.fetchall()
        if not exists:
            abort(404, message = 'Product Line does not exist...')

    def delete(cls):
        args = request.args
        if not args or not args.get('product_id'):
            return 'This method requires product_id', 400
        product_id = args.get('product_id')
        # Products.abort_if_product_doesnt_exist(product_id)
        
        try:
            cursor.execute(f"DELETE FROM orderdetails WHERE productCode='{product_id}'")
            cursor.execute(f"DELETE FROM products WHERE productCode='{product_id}'")
            db.commit()
            return 'Product deleted!', 200
        except Exception as err:
            return err, 400
        

    def post(cls):
        # Check if body is JSON
        content_type = request.headers.get('Content-Type')
        body = ''
        if (content_type == 'application/json'):
            body = request.json
        else:
            return 'Content-Type not supported!', 400
        
        # Post for getting a single product
        if len(body) == 1:
            product_id = body['productCode']
            # Products.abort_if_product_doesnt_exist(product_id)
            try:
                cursor.execute(f"SELECT * FROM products WHERE productCode='{product_id}'")
                product = cursor.fetchall()[0]
                product_obj = {
                    "productCode": product[0],
                    "productName": product[1],
                    "productLine": product[2],
                    "productScale": product[3],
                    "productVendor": product[4],
                    "productDescription": product[5],
                    "quantityInStock": product[6],
                    "buyPrice": float(product[7]),
                    "MSRP": float(product[8])
                }
                return product_obj, 200
            except Exception as err:
                return err, 400
        
        # if not ("productCode" in body and "productName" in body and "productLine" in body
        #         and "productScale" in body and "productVendor" in body
        #         and "productDescription" in body and "quantityInStock" in body
        #         and "buyPrice" in body and "MSRP" in body):
        #     return 'Missing a field in request body...', 400

        # Check if productCode exists
        # Products.abort_if_product_exists(body['productCode'])
        # Check if productLine exists
        # Products.abort_if_product_line_doesnt_exist(body['productLine'])

        try:
            sql_command = ("INSERT INTO products(productCode, productName, productLine, productScale,"
                       " productVendor, productDescription, quantityInStock, buyPrice, MSRP)"
                    f" VALUES ('{body['productCode']}', '{body['productName']}', '{body['productLine']}',"
                    f"'{body['productScale']}', '{body['productVendor']}', '{body['productDescription']}', "
                    f"{body['quantityInStock']}, {body['buyPrice']}, {body['MSRP']});")
            cursor.execute(sql_command)
            db.commit()
            return 'New product added!', 200
        except Exception as err:
            return err, 400
        
    
    def put(cls):
        args = request.args
        if not args or not args.get('product_id'):
            return 'This method requires product_id', 400
        product_id = args.get('product_id')
        # Products.abort_if_product_doesnt_exist(product_id)

        content_type = request.headers.get('Content-Type')
        body = ''
        if (content_type == 'application/json'):
            body = request.json
        else:
            return 'Content-Type not supported!', 400
        
        for field in body:
            # if field == 'productLine':
            #     Products.abort_if_product_line_doesnt_exist(body[field])
            
            sql_command = f"UPDATE products SET {field}='{body[field]}' WHERE productCode='{product_id}'"
            if field == 'quantityInStock' or field == 'buyPrice' or field == 'MSRP':
                sql_command = f"UPDATE products SET {field}={body[field]} WHERE productCode='{product_id}'"
            
            try:
                cursor.execute(sql_command)
                db.commit()
            except Exception as err:
                return err, 400
            
        return "Product updated", 200
    #Add try and except block, it will ease your checking if something exists.
