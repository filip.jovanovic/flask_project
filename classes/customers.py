import os
import sys
sys.path.append(os.getcwd())

from datetime import datetime, date
from database.db import cursor, db
from flask_restful import Resource, request, abort
from classes.employees import Employees
import random, string


class Customers(Resource):

    def get(cls):
        args = request.args
        if not args or not args.get('employee_id'):
            return 'This method requires employee_id', 400
        employee_id = args.get('employee_id')
        Employees.abort_if_employee_doesnt_exist(employee_id)

        sql_command = f"SELECT * FROM customers WHERE salesRepEmployeeNumber={employee_id}"
        cursor.execute(sql_command)
        sql_result = cursor.fetchall()
        result = []
        for customer in sql_result:
            customer_obj = {
                "customerNumber": customer[0],
                "customerName": customer[1],
                "customerLastName": customer[2],
                "customerFirstName": customer[3],
                "phone": customer[4],
                "addressLine1": customer[5],
                "addressLine2": customer[6],
                "city": customer[7],
                "state": customer[8],
                "postalCode": customer[9],
                "country": customer[10],
                "salesRepEmployeeNumber": customer[11],
                "creditLimit": float(customer[12])
            }
            result.append(customer_obj)
        return result, 200

    @staticmethod
    def abort_if_order_doesnt_exist(order_id):
        cursor.execute('SELECT * FROM orders WHERE orderNumber=%s', (order_id, ))
        exists = cursor.fetchall()
        if not exists:
            abort(404, message = 'Order does not exist...')

    def post(cls):
        content_type = request.headers.get('Content-Type')
        body = ''
        if (content_type == 'application/json'):
            body = request.json
        else:
            return 'Content-Type not supported!', 400
        
        if len(body) == 1:
            # Add new payment
            if not "orderNumber" in body:
                return 'Missing field in request body...', 400
            
            order_num = body['orderNumber']
            Customers.abort_if_order_doesnt_exist(order_num)
            cursor.execute(f"SELECT customerNumber FROM orders WHERE orderNumber={order_num}")
            customer_num = cursor.fetchall()[0][0]
            cursor.execute(f"SELECT quantityOrdered FROM orderdetails WHERE orderNumber={order_num}")
            qunatity_ordered = cursor.fetchall()[0][0]
            cursor.execute(f"SELECT priceEach FROM orderdetails WHERE orderNumber={order_num}")
            price_each = cursor.fetchall()[0][0]
            amount = qunatity_ordered * price_each
            check_num = ''.join(random.choices(string.ascii_letters + string.digits, k=7))
            payment_date = date.today()

            # Insert this...
            sql_command = (f"INSERT INTO payments VALUES ({customer_num},"
                           f" '{check_num}', '{payment_date}', {amount});")
            cursor.execute(sql_command)
            db.commit()
            return "Payment for customer added...", 200
        
        # Get payment
        if not ("customerNumber" in body and "date" in body):
            return 'Missing field in request body...', 400
        
        customerNumber = body['customerNumber']
        datum = body['date']

        # Validate date format
        try:
            res = bool(datetime.strptime(datum, "%Y-%m-%d")) 
        except:
            return "Wrong date format, shuold be yyyy-mm-dd", 400

        sql_command = f"SELECT * FROM payments WHERE customerNumber={customerNumber} AND paymentDate>'{datum}'"
        cursor.execute(sql_command)
        sql_result = cursor.fetchall()
        result = []
        for payment in sql_result:
            payment_object = {
                "customerNumber": payment[0],
                "checkNumber": payment[1],
                "paymentDate": str(payment[2]),
                "amount": float(payment[3]),
            }
            result.append(payment_object)
        return result, 200
