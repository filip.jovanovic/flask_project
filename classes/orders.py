import os
import sys
sys.path.append(os.getcwd())

from database.db import cursor, db
from flask_restful import Resource, request, abort
from classes.products import Products


class Orders(Resource):

    @staticmethod
    def abort_if_customer_doesnt_exist(customer_id):
        cursor.execute('SELECT * FROM customers WHERE customerNumber=%s', (customer_id, ))
        exists = cursor.fetchall()
        if not exists:
            abort(404, message = 'Customer does not exist...')
    
    def post(cls):
        # Check if body is in JSON format
        content_type = request.headers.get('Content-Type')
        body = ''
        if (content_type == 'application/json'):
            body = request.json
        else:
            return 'Content-Type not supported!', 400
        
        # Check if body contains all required fields
        # if not ("customerNumber" in body and "productCode" in body
        #         and "quantityOrdered" in body and "priceEach" in body
        #         and "orderLineNumber" in body and "orderDate" in body
        #         and "requiredDate" in body and 'shippedDate' in body
        #         and 'status' in body and 'comments' in body):
        #     return 'Missing a field in request body...', 400

        # Check if customer and product exist
        # Orders.abort_if_customer_doesnt_exist(body['customerNumber'])
        # Products.abort_if_product_doesnt_exist(body['productCode'])

        # Check if there are enough items in stock
        cursor.execute(f"SELECT quantityInStock from products WHERE productCode='{body['productCode']}'")
        quantity_in_stock = cursor.fetchall()[0][0]
        quantity_ordered = body['quantityOrdered']
        if quantity_in_stock < quantity_ordered:
            return "Not enough items in stock...", 200
        
        # newOrderNumber = max(orderNumber) + 1
        cursor.execute("SELECT MAX(orderNumber) FROM orders;")
        order_number = cursor.fetchall()[0][0] + 1

        try:
            # Insert new order
            sql_inset_order = (f"INSERT INTO orders VALUES ({order_number}, '{body['orderDate']}',"
                            f" '{body['requiredDate']}', '{body['shippedDate']}',"
                            f" '{body['status']}', '{body['comments']}', {body['customerNumber']});")
            cursor.execute(sql_inset_order)
            db.commit()

             # Insert orderdetails
            sql_insert_order_details = (f"INSERT INTO orderdetails VALUES ({order_number},"
                                        f" '{body['productCode']}', {quantity_ordered},"
                                        f" {body['priceEach']}, {body['orderLineNumber']});")
            cursor.execute(sql_insert_order_details)
            print(sql_insert_order_details)
            db.commit()

            return "Order added", 200
        except Exception as err:
            return err, 400
