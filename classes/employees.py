import os
import sys
sys.path.append(os.getcwd())

from database.db import cursor, db
from flask import jsonify
from flask_restful import Resource, request, abort

class Employees(Resource):

    def get(cls):
        cursor.execute('SELECT * FROM employees;')
        sql_result = cursor.fetchall()
        result = []
        for employee in sql_result:
            employee_obj = {
                "employeeNumber": employee[0],
                "lastName": employee[1],
                "firstName": employee[2],
                "extension": employee[3],
                "email": employee[4],
                "officeCode": employee[5],
                "reportsTo": employee[6],
                "jobTitle": employee[7],
            }
            result.append(employee_obj)
        return result, 200

    @staticmethod
    def abort_if_employee_doesnt_exist(employee_id):
        cursor.execute('SELECT * FROM employees WHERE employeeNumber=%s', (employee_id, ))
        exists = cursor.fetchall()
        if not exists:
            abort(404, message = 'Employee does not exist...')

    @staticmethod
    def abort_if_office_doesnt_exist(office_code):
        cursor.execute('SELECT * FROM offices WHERE officeCode=%s', (office_code, ))
        exists = cursor.fetchall()
        if not exists:
            abort(404, message = 'Office code does not exist...')
    
    def delete(cls):
        args = request.args
        if not args or not args.get('employee_id'):
            return 'This method requires employee_id', 400
        employee_id = args.get('employee_id')
        # Employees.abort_if_employee_doesnt_exist(employee_id)
        
        try:
            # cursor.execute('UPDATE employees SET reportsTo=NULL WHERE reportsTo=%s', (employee_id, ))
            # cursor.execute('UPDATE customers SET salesRepEmployeeNumber=NULL WHERE salesRepEmployeeNumber=%s', (employee_id, ))
            #Two lines above are not needed, it works the same with them or without them
            cursor.execute("DELETE FROM employees WHERE employeeNumber=%s", (employee_id, ))
            db.commit()
            return 'Employee deleted!', 200
        except Exception as err:
            return err, 400
        
    
    def post(cls):
        content_type = request.headers.get('Content-Type')
        body = ''
        if (content_type == 'application/json'):
            body = request.json
        else:
            return 'Content-Type not supported!', 400
        
        # if not ("lastName" in body and "firstName" in body and "extension" in body and "email" in body
        #         and "officeCode" in body and "reportsTo" in body and "jobTitle" in body):
        #     return 'Missing a field in request body...', 400
        
        # Check if reportsTo employee exists
        # if body['reportsTo'] != "NULL":
        #     Employees.abort_if_employee_doesnt_exist(body['reportsTo'])
        # Check if officeCode exists
        # Employees.abort_if_office_doesnt_exist(body['officeCode'])

        cursor.execute("SELECT MAX(employeeNumber) FROM employees;")
        new_employee_number = cursor.fetchall()[0][0] + 1

        try:
            sql_command = (f"INSERT INTO employees(employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle)"
                    f"VALUES ({new_employee_number}, '{body['lastName']}', '{body['firstName']}', '{body['extension']}',"
                    f"'{body['email']}', '{body['officeCode']}', {body['reportsTo']}, '{body['jobTitle']}');")
            cursor.execute(sql_command)
            db.commit()
            return 'New employee added!', 200
        except Exception as err:
            return err, 400
        

    def put(cls):
        args = request.args
        if not args or not args.get('employee_id'):
            return 'This method requires employee_id', 400
        employee_id = args.get('employee_id')
        # Employees.abort_if_employee_doesnt_exist(employee_id)

        content_type = request.headers.get('Content-Type')
        body = ''
        if (content_type == 'application/json'):
            body = request.json
        else:
            return 'Content-Type not supported!', 400
        
        for field in body:
            # if field == 'officeCode':
            #     Employees.abort_if_office_doesnt_exist(body[field])
            # if field == 'reportsTo' and body[field] != "NULL":
            #     Employees.abort_if_employee_doesnt_exist(body[field])
            
            sql_command = f"UPDATE employees SET {field}='{body[field]}' WHERE employeeNumber={employee_id}"
            if field == 'reportsTo':
                sql_command = f"UPDATE employees SET {field}={body[field]} WHERE employeeNumber={employee_id}"
            try:
                cursor.execute(sql_command)
                db.commit()
                return "Employee updated", 200
            except Exception as err:
                return err, 400
    
    
    #Add try and except block, it will ease your checking if something exists.
    #Use return instead of abort. In except block return error message and status code
