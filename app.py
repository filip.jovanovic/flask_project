from flask import Flask ,request
from flask_restful import Api

from classes.employees import Employees
from classes.customers import Customers
from classes.products import Products
from classes.orders import Orders

app = Flask(__name__)
api = Api(app)


api.add_resource(Employees, '/employees')
api.add_resource(Customers, '/employees/customers', '/customer/payment')
api.add_resource(Products, '/products')
api.add_resource(Orders, '/customer/order')


if __name__ == "__main__":
    app.run(debug=True)



###General comments###
#Remove unused files - done
#Add requirments.txt with libraries - done
#Add try and except block, it will ease your checking if something exists. - done
#Use return instead of abort. In except block return error message and status code - done